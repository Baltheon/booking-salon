package com.booking.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.utils.Formatter;

public class PrintService {
    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        return serviceList.stream().map(Service::getServiceName).collect(Collectors.joining(", "));
    }

    public static void showRecentReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out
                .println(
                        "+----------------------------------------------------------------------------------------------------------------------------+");
        System.out.printf("| %-3s | %-6s | %-13s | %-47s | %-15s | %-10s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out
                .println(
                        "+----------------------------------------------------------------------------------------------------------------------------+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-3s | %-6s | %-13s | %-47s | %15s | %-10s | %-10s |\n",
                        num, reservation.getReservationId(), reservation.getCustomer().getName(),
                        printServices(reservation.getServices()),
                        Formatter.getCurrencyFormat(reservation.getReservationPrice()),
                        reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
        System.out
                .println(
                        "+----------------------------------------------------------------------------------------------------------------------------+");
    }

    public static void showAllCustomer(List<Person> personList) {
        AtomicInteger num = new AtomicInteger(1);
        System.out
                .println("+---------------------------------------------------------------------------------+");
        System.out.printf("| %-3s | %-7s | %-11s | %-15s | %-15s | %-13s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out
                .println("+---------------------------------------------------------------------------------+");
        personList.stream()
                .filter(person -> person instanceof Customer)
                .map(person -> (Customer) person)
                .forEach(customer -> {
                    System.out.printf("| %-3s | %-7s | %-11s | %-15s | %-15s | %13s |\n", num.getAndIncrement(),
                            customer.getId(),
                            customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(),
                            Formatter.getCurrencyFormat(customer.getWallet()));
                });
        System.out
                .println("+---------------------------------------------------------------------------------+");
    }

    public static void showAllEmployee(List<Person> personList) {
        AtomicInteger num = new AtomicInteger(1);
        System.out
                .println("+------------------------------------------------------------+");
        System.out.printf("| %-3s | %-7s | %-11s | %-15s | %-10s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out
                .println("+------------------------------------------------------------+");
        personList.stream()
                .filter(person -> person instanceof Employee)
                .map(person -> (Employee) person)
                .forEach(employee -> {
                    System.out.printf("| %-3s | %-7s | %-11s | %-15s | %10s |\n", num.getAndIncrement(),
                            employee.getId(),
                            employee.getName(), employee.getAddress(), employee.getExperience());
                });
        System.out
                .println("+------------------------------------------------------------+");

    }

    public static void showReservationHistory(List<Reservation> reservationList) {
        AtomicInteger num = new AtomicInteger(1);
        double totalPrice = 0.0;
        System.out
                .println(
                        "+---------------------------------------------------------------------------------------------------------------+");
        System.out.printf("| %-3s | %-6s | %-13s | %-47s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out
                .println(
                        "+---------------------------------------------------------------------------------------------------------------+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finished")) {
                totalPrice += reservation.getReservationPrice();
            }
            System.out.printf("| %-3s | %-6s | %-13s | %-47s | %15s | %-10s |\n", num.getAndIncrement(),
                    reservation.getReservationId(), reservation.getCustomer().getName(),
                    printServices(reservation.getServices()),
                    Formatter.getCurrencyFormat(reservation.getReservationPrice()),
                    reservation.getWorkstage());
        }
        System.out
                .println(
                        "+---------------------------------------------------------------------------------------------------------------+");
        System.out.printf("| %-78s | %28s |\n", "Total Keuntungan", Formatter.getCurrencyFormat(totalPrice));
        System.out
                .println(
                        "+---------------------------------------------------------------------------------------------------------------+");
    }

    public static void showAllServices(List<Service> serviceList) {
        AtomicInteger num = new AtomicInteger(1);
        System.out.println("+--------------------------------------------------------+");
        System.out.printf("| %-3s | %-7s | %-20s | %-15s |\n", "No.", "ID", "Nama", "Harga");
        System.out.println("+--------------------------------------------------------+");
        serviceList.stream()
                .forEach(service -> System.out.printf("| %-3s | %-7s | %-20s | %15s |\n", num.getAndIncrement(),
                        service.getServiceId(), service.getServiceName(),
                        Formatter.getCurrencyFormat(service.getPrice())));
        System.out.println("+--------------------------------------------------------+");

    }
}
