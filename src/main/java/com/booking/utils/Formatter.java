package com.booking.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Formatter {
    public static String getCurrencyFormat(double value) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        return "Rp " + decimalFormat.format(value);
    }

}
