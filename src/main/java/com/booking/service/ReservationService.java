package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.utils.Formatter;
import com.booking.utils.Generator;

public class ReservationService {
    private static Scanner input = new Scanner(System.in);

    public static void createReservation(List<Person> personList, List<Reservation> reservationList,
            List<Service> serviceList) {
        String customerID, employeeID, serviceID, addService;
        List<Service> reservedServiceList = new ArrayList<Service>();
        PrintService.showAllCustomer(personList);
        do {
            System.out.println("Silahkan masukkan customer ID :");
            customerID = input.nextLine();
        } while (!ValidationService.validateCustomerId(customerID, personList));
        PrintService.showAllEmployee(personList);
        do {
            System.out.println("Silahkan masukkan employee ID :");
            employeeID = input.nextLine();
        } while (!ValidationService.validateEmployeeId(employeeID, personList));
        PrintService.showAllServices(serviceList);
        do {
            do {
                System.out.println("Silahkan masukkan service ID :");
                serviceID = input.nextLine();
            } while (!ValidationService.validateServiceId(serviceID, serviceList, reservedServiceList));
            reservedServiceList.add(getServiceByServiceId(serviceID, serviceList));
            System.out.println("Ingin menambahkan service lain? (Y/T)");
            addService = input.nextLine();
        } while (!ValidationService.validateInput(addService, serviceList, reservedServiceList));
        String reservationId = Generator.generateReservationId(reservationList);
        Customer customer = getCustomerByCustomerId(customerID, personList);
        Employee employee = getEmployeeByEmployeeId(employeeID, personList);
        Reservation reservation = new Reservation(reservationId, customer, employee, reservedServiceList, "In Process");
        reservationList.add(reservation);
        System.out.println("Booking berhasil!");
        double totalPrice = reservation.getReservationPrice();
        System.out.println("Total biaya booking:" + Formatter.getCurrencyFormat(totalPrice) + "\n");
    }

    public static Customer getCustomerByCustomerId(String customerId, List<Person> personList) {
        return (Customer) personList.stream().filter(person -> person.getId().equalsIgnoreCase(customerId)).findFirst()
                .orElse(null);
    }

    public static Employee getEmployeeByEmployeeId(String employeeId, List<Person> personList) {
        return (Employee) personList.stream().filter(person -> person.getId().equalsIgnoreCase(employeeId)).findFirst()
                .orElse(null);
    }

    public static Service getServiceByServiceId(String serviceId, List<Service> serviceList) {
        return serviceList.stream().filter(service -> service.getServiceId().equalsIgnoreCase(serviceId)).findFirst()
                .orElse(null);
    }

    public static Reservation getReservationById(String reservationID, List<Reservation> reservationList) {
        return reservationList.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationID)).findFirst()
                .orElse(null);
    }

    public static void editReservationWorkstage(List<Reservation> reservationList) {
        String reservationID, updatedWorkstage;
        do {
            System.out.println("Silahkan masukkan reservation ID :");
            reservationID = input.nextLine();
        } while (!ValidationService.validateReservationID(reservationID, reservationList));
        do {
            System.out.println("Selesaikan reservasi (Finish/Cancel):");
            updatedWorkstage = input.nextLine();
        } while (!ValidationService.validateUpdatedWorkstage(updatedWorkstage));
        Reservation reservation = getReservationById(reservationID, reservationList);
        switch (updatedWorkstage) {
            case "finish":
                reservation.setWorkstage("Finished");
                break;
            case "cancel":
                reservation.setWorkstage("Canceled");
                break;
        }
        System.out.println("Reservasi dengan ID " + reservationID + " telah " + updatedWorkstage + "!\n");

    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
