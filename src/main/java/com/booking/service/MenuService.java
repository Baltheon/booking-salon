package com.booking.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<Reservation>() {
        {
            add(new Reservation("Rsv-01", (Customer) personList.get(0), (Employee) personList.get(4),
                    Arrays.asList(serviceList.get(0), serviceList.get(1)), "In Process"));
            add(new Reservation("Rsv-02", (Customer) personList.get(1), (Employee) personList.get(5),
                    Arrays.asList(serviceList.get(2), serviceList.get(3)), "Finished"));
            add(new Reservation("Rsv-03", (Customer) personList.get(0), (Employee) personList.get(4),
                    Arrays.asList(serviceList.get(0), serviceList.get(1), serviceList.get(2)), "Canceled"));
        }
    };
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = { "Show Data", "Create Reservation", "Complete/cancel Reservation", "Exit" };
        String[] subMenuArr = { "Show Recent Reservation", "Show Customer", "Show Available Employee",
                "Show Reservation History",
                "Back to main menu" };

        int optionMainMenu;
        int optionSubMenu;

        boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                // panggil fitur tampilkan recent reservation
                                PrintService.showRecentReservation(reservationList);
                                break;
                            case 2:
                                // panggil fitur tampilkan semua customer
                                PrintService.showAllCustomer(personList);
                                break;
                            case 3:
                                // panggil fitur tampilkan semua employee
                                PrintService.showAllEmployee(personList);
                                break;
                            case 4:
                                // panggil fitur tampilkan history reservation + total keuntungan
                                PrintService.showReservationHistory(reservationList);
                                break;
                            case 0:
                                backToSubMenu = true;
                                break;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    // panggil fitur menambahkan reservation
                    ReservationService.createReservation(personList, reservationList, serviceList);
                    break;
                case 3:
                    // panggil fitur mengubah workstage menjadi finish/cancel
                    PrintService.showRecentReservation(reservationList);
                    ReservationService.editReservationWorkstage(reservationList);
                    break;
                case 0:
                    backToMainMenu = true;
                    break;
            }
        } while (!backToMainMenu);
    }
}
