package com.booking.service;

import java.util.Arrays;
import java.util.List;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static boolean validateInput(String value, List<Service> serviceList, List<Service> reservedServiceList) {
        List<String> allowedInput = Arrays.asList("Y", "T");
        if (value.isEmpty()) {
            System.out.println("Input tidak boleh kosong!");
            return false;
        }
        if (!allowedInput.contains(value.toUpperCase())) {
            System.out.println("Input hanya menerima Y atau T!");
            return false;
        }
        if (!validateService(serviceList, reservedServiceList)) {
            return true;
        }
        if (value.equalsIgnoreCase("Y")) {
            return false;
        }
        return true;
    }

    public static boolean validateService(List<Service> serviceList, List<Service> reservedServiceList) {
        if (reservedServiceList.size() == serviceList.size()) {
            System.out.println("Semua service telah dipilih!");
            return false;
        }
        return true;
    }

    public static boolean validateCustomerId(String customerId, List<Person> personList) {
        if (customerId.isEmpty()) {
            System.out.println("Customer ID tidak boleh kosong!");
            return false;
        }
        Person customer = personList.stream()
                .filter(person -> person.getId().equalsIgnoreCase(customerId))
                .findFirst().orElse(null);
        if (customer == null) {
            System.out.println("Customer yang dicari tidak tersedia!");
            return false;
        }
        return true;
    }

    public static boolean validateEmployeeId(String employeeId, List<Person> personList) {
        if (employeeId.isEmpty()) {
            System.out.println("Employee ID tidak boleh kosong!");
            return false;
        }
        Person employee = personList.stream()
                .filter(person -> person.getId().equalsIgnoreCase(employeeId))
                .findFirst().orElse(null);
        if (employee == null) {
            System.out.println("Employee yang dicari tidak tersedia!");
            return false;
        }
        return true;
    }

    public static boolean validateServiceId(String serviceId, List<Service> serviceList,
            List<Service> reservedServiceList) {
        if (serviceId.isEmpty()) {
            System.out.println("Service ID tidak boleh kosong!");
            return false;
        }
        Service Service = serviceList.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(serviceId))
                .findFirst().orElse(null);
        if (Service == null) {
            System.out.println("Service yang dicari tidak tersedia!");
            return false;
        }
        if (reservedServiceList.contains(Service)) {
            System.out.println("Service sudah dipilih!");
            return false;
        }
        return true;
    }

    public static boolean validateReservationID(String reservationID, List<Reservation> reservationList) {
        if (reservationID.isEmpty()) {
            System.out.println("Reservation ID tidak boleh kosong!");
            return false;
        }
        Reservation reservation = reservationList.stream()
                .filter(item -> item.getReservationId().equalsIgnoreCase(reservationID))
                .findFirst().orElse(null);
        if (reservation == null) {
            System.out.println("Reservation yang dicari tidak tersedia!");
            return false;
        }
        if (!reservation.getWorkstage().equalsIgnoreCase("In Process")) {
            System.out.println("Reservation yang dicari sudah selesai!");
            return false;
        }
        return true;
    }

    public static boolean validateUpdatedWorkstage(String updatedWorkstage) {
        if (updatedWorkstage.isEmpty()) {
            System.out.println("Status reservasi tidak boleh kosong!");
            return false;
        }
        List<String> allowedWorkstage = Arrays.asList("finish", "cancel");
        if (!allowedWorkstage.contains(updatedWorkstage.toLowerCase())) {
            System.out.println("Status reservasi hanya menerima Finish atau Cancel!");
            return false;
        }
        return true;
    }
}
