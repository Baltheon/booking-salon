package com.booking.utils;

import java.util.List;

import com.booking.models.Reservation;

public class Generator {
    public static String generateReservationId(List<Reservation> reservationList) {
        String prefix = "Rsv-";
        String lastID;
        if (reservationList.size() == 0) {
            return prefix + "01";
        } else {
            lastID = reservationList.get(reservationList.size() - 1).getReservationId().substring(4);
            return prefix + String.format("%02d", Integer.parseInt(lastID) + 1);
        }

    }

}
