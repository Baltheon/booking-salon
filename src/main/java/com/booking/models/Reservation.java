package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    // workStage (In Process, Finished, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.workstage = workstage;
        this.reservationPrice = calculateReservationPrice();
    };

    private double calculateReservationPrice() {
        double reservationPrice = 0.0;
        if (workstage.equalsIgnoreCase("Canceled")) {
            return reservationPrice;
        }
        for (Service service : services) {
            reservationPrice += service.getPrice();
        }
        String membershipType = customer.getMember().getMembershipName();
        switch (membershipType.toLowerCase()) {
            case "silver":
                return reservationPrice * 0.95;
            case "gold":
                return reservationPrice * 0.9;
            default:
                return reservationPrice;
        }
    }

    public void setWorkstage(String workstage) {
        this.workstage = workstage;
        this.reservationPrice = calculateReservationPrice();
    }
}
